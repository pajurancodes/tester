# tester

A library created and used for testing purposes only.

# Give Up GitHub

This project has given up GitHub.  ([See Software Freedom Conservancy's *Give Up  GitHub* site for details](https://GiveUpGitHub.org).)

Join us; you can [give up GitHub](https://GiveUpGitHub.org) too!